﻿namespace BnsPatcher.Patcher.Extensions
{
    public static class BladeMasterSpiritExtension
    {
        public static DatEditor Remove4ThHitFallingStars(this DatEditor datEditor)
        {
            datEditor.BladeMasterSpiritXml = datEditor.BladeMasterSpiritXml.Replace("<result context-2=\"103493\" control-mode=\"classic\" />", "");
            datEditor.BladeMasterSpiritXml = datEditor.BladeMasterSpiritXml.Replace("<result context-2=\"103493\" control-mode=\"bns\" />", "");
            datEditor.BladeMasterSpiritXml = datEditor.BladeMasterSpiritXml.Replace("<result context-2=\"103513\" control-mode=\"classic\" />", "");
            datEditor.BladeMasterSpiritXml = datEditor.BladeMasterSpiritXml.Replace("<result context-2=\"103513\" control-mode=\"bns\" />", "");

            datEditor.BladeMasterSpiritSimpleModeXml = datEditor.BladeMasterSpiritSimpleModeXml.Replace("<result context-2=\"103493\" control-mode=\"classic\" />", "");
            datEditor.BladeMasterSpiritSimpleModeXml = datEditor.BladeMasterSpiritSimpleModeXml.Replace("<result context-2=\"103493\" control-mode=\"bns\" />", "");
            datEditor.BladeMasterSpiritSimpleModeXml = datEditor.BladeMasterSpiritSimpleModeXml.Replace("<result context-2=\"103513\" control-mode=\"classic\" />", "");
            datEditor.BladeMasterSpiritSimpleModeXml = datEditor.BladeMasterSpiritSimpleModeXml.Replace("<result context-2=\"103513\" control-mode=\"bns\" />", "");

            return datEditor;
        }

        public static DatEditor MoveSwordFallToLmb(this DatEditor datEditor)
        {
            datEditor.BladeMasterSpiritXml = datEditor.BladeMasterSpiritXml.Replace("<condition skill=\"103310\" />", "<condition skill=\"103212\" /> <result context-1=\"103212\" control-mode=\"classic\" /> <result context-1=\"103212\" control-mode=\"bns\" /> </decision> </layer> <layer> <decision> <condition skill=\"103210\" /> <result context-1=\"103210\" control-mode=\"classic\" /> <result context-1=\"103210\" control-mode=\"bns\" /> </decision> </layer> <layer> <decision> <condition skill=\"103310\" />");
            datEditor.BladeMasterSpiritXml = datEditor.BladeMasterSpiritXml.Replace("<result context-3=\"103210\" control-mode=\"classic\" />", "<result context-1=\"103210\" control-mode=\"classic\" />");
            datEditor.BladeMasterSpiritXml = datEditor.BladeMasterSpiritXml.Replace("<result context-3=\"103210\" control-mode=\"bns\" />", "<result context-1=\"103210\" control-mode=\"bns\" />");
            datEditor.BladeMasterSpiritXml = datEditor.BladeMasterSpiritXml.Replace("<result context-3=\"103212\" control-mode=\"classic\" />", "<result context-1=\"103212\" control-mode=\"classic\" />");
            datEditor.BladeMasterSpiritXml = datEditor.BladeMasterSpiritXml.Replace("<result context-3=\"103212\" control-mode=\"bns\" />", "<result context-1=\"103212\" control-mode=\"bns\" />");
            return datEditor;
        }

        public static DatEditor OptimizeSimpleMode(this DatEditor datEditor)
        {
            datEditor.BladeMasterSpiritSimpleModeXml = datEditor.BladeMasterSpiritSimpleModeXml.Replace("<result context-2=\"103210\" control-mode=\"bns\" />", "");
            datEditor.BladeMasterSpiritSimpleModeXml = datEditor.BladeMasterSpiritSimpleModeXml.Replace("<result context-2=\"103253\" control-mode=\"bns\" />", "");
            datEditor.BladeMasterSpiritSimpleModeXml = datEditor.BladeMasterSpiritSimpleModeXml.Replace("<result context-2=\"103252\" control-mode=\"bns\" />", "");
            datEditor.BladeMasterSpiritSimpleModeXml = datEditor.BladeMasterSpiritSimpleModeXml.Replace("<result context-2=\"103251\" control-mode=\"bns\" />", "");
            datEditor.BladeMasterSpiritSimpleModeXml = datEditor.BladeMasterSpiritSimpleModeXml.Replace("<result context-2=\"103250\" control-mode=\"bns\" />", "");

            datEditor.BladeMasterSpiritSimpleModeXml = datEditor.BladeMasterSpiritSimpleModeXml.Replace("<result context-2=\"103490\" control-mode=\"bns\" />", "<result context-2=\"103490\" control-mode=\"bns\" /> </decision> </layer> <layer> <decision> <condition skill=\"103210\" /> <result context-2=\"103210\" control-mode=\"classic\" /> <result context-2=\"103210\" control-mode=\"bns\" /> </decision> </layer> <layer> <decision> <condition skill=\"103310\" /> <result context-2=\"103310\" control-mode=\"classic\" /> <result context-2=\"103310\" control-mode=\"bns\" /> </decision> </layer> <layer> <decision> <condition skill=\"103253\" /> <result context-2=\"103253\" control-mode=\"classic\" /> <result context-2=\"103253\" control-mode=\"bns\" /> </decision> </layer> <layer> <decision> <condition skill=\"103252\" /> <result context-2=\"103252\" control-mode=\"classic\" /> <result context-2=\"103252\" control-mode=\"bns\" /> </decision> </layer> <layer> <decision> <condition skill=\"103251\" /> <result context-2=\"103251\" control-mode=\"classic\" /> <result context-2=\"103251\" control-mode=\"bns\" /> </decision> </layer> <layer> <decision> <condition skill=\"103250\" /> <result context-2=\"103250\" control-mode=\"classic\" /> <result context-2=\"103250\" control-mode=\"bns\" />");
            return datEditor;
        }

        public static DatEditor AddBladeStormToSf3(this DatEditor datEditor)
        {
            datEditor.BladeMasterSpiritSimpleModeXml = datEditor.BladeMasterSpiritSimpleModeXml.Replace("<result context-2=\"103210\" control-mode=\"bns\" />", "<result context-2=\"103210\" control-mode=\"bns\" /> </decision> </layer> <layer> <decision> <condition skill=\"103310\" /> <result context-2=\"103310\" control-mode=\"classic\" /> <result context-2=\"103310\" control-mode=\"bns\" />");
            return datEditor;
        }
    }
}
