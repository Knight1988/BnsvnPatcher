﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace BnsPatcher.Patcher.Extensions
{
    public static class ClientConfig2Extension
    {
        public static DatEditor ShowDps(this DatEditor datEditor)
        {
            var names = new[]
            {
                "showtype-public-zone", "showtype-party-4-dungeon-and-cave", "showtype-party-6-dungeon-and-cave",
                "showtype-field-zone", "showtype-classic-field-zone", "showtype-faction-battle-field-zone",
                "showtype-jackpot-boss-zone"
            };
            foreach (var name in names)
            {
                var regex = new Regex($"<option name=\"{name}\" value=\"[0-9.]+\" \\/>");
                datEditor.ClientConfig2 = regex.Replace(datEditor.ClientConfig2, $"<option name=\"{name}\" value=\"2\" />");
            }
            return datEditor;
        }

        public static DatEditor NoChatBan(this DatEditor datEditor)
        {
            var names = new[]
            {
                "input-papering-check-low-penalty-duration", "input-papering-check-midium-penalty-duration",
                "input-papering-check-high-penalty-duration"
            };
            foreach (var name in names)
            {
                var regex = new Regex($"<option name=\"{name}\" value=\"[0-9.]+\" \\/>");
                datEditor.ClientConfig2 = regex.Replace(datEditor.ClientConfig2, $"<option name=\"{name}\" value=\"3.000000\" />");
            }
            return datEditor;
        }

        public static DatEditor FastExtract(this DatEditor datEditor)
        {
            var dic = new Dictionary<string, string>()
            {
                {"delay-postproc-time", "0.100000" },
                {"delay-ui-time", "0.100000" },
                {"train-complete-delay-time", "0.100000" },
                {"rapid-decompose-duration", "0.100000" },
                {"decompose-complete-delay", "0.100000" },
                {"self-restraint-gauge-time", "0.010000" },
                {"item-transform-progressing-particle-duration", "0.1" },
            };
            foreach (var keyValue in dic)
            {
                var regex = new Regex($"<option name=\"{keyValue.Key}\" value=\"[0-9.]+\" \\/>");
                datEditor.ClientConfig2 = regex.Replace(datEditor.ClientConfig2, $"<option name=\"{keyValue.Key}\" value=\"{keyValue.Value}\" />");
            }
            return datEditor;
        }

        public static DatEditor IncreaseDps(this DatEditor datEditor)
        {
            var dic = new Dictionary<string, string>()
            {
                {"pending-time", "0.180" },
                {"pressed-key-tick-time", "0.09" },
                {"charge-key-tick-time", "0.09" },
                {"ignore-mouse-press-time", "0" },
            };
            foreach (var keyValue in dic)
            {
                var regex = new Regex($"<option name=\"{keyValue.Key}\" value=\"[0-9.]+\" \\/>");
                datEditor.ClientConfig2 = regex.Replace(datEditor.ClientConfig2, $"<option name=\"{keyValue.Key}\" value=\"{keyValue.Value}\" />");
            }
            return datEditor;
        }
    }
}
