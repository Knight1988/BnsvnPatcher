﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BnsPatcher.Patcher
{
    public class DatEditor
    {
        public string DatFolder { get; set; }
        private readonly Dictionary<string, byte[]> _xml64;
        private readonly BNSDat.BNSDat _bnsDat;
        public Dictionary<string, string> MyDictionary { get; } = new Dictionary<string, string>();

        public string ClientConfig2
        {
            get{ return Encoding.UTF8.GetString(_xml64["client.config2.xml"]); }
            set { _xml64["client.config2.xml"] = Encoding.UTF8.GetBytes(value); }
        }

        public string BladeMasterSpiritXml
        {
            get{ return Encoding.UTF8.GetString(_xml64["skill3_contextscriptdata_blademaster.xml"]); }
            set { _xml64["skill3_contextscriptdata_blademaster.xml"] = Encoding.UTF8.GetBytes(value); }
        }

        public string BladeMasterSpiritSimpleModeXml
        {
            get{ return Encoding.UTF8.GetString(_xml64["skill3_contextscriptdata_blademaster_contextsimplemode.xml"]); }
            set { _xml64["skill3_contextscriptdata_blademaster_contextsimplemode.xml"] = Encoding.UTF8.GetBytes(value); }
        }

        public DatEditor(string datFolder)
        {
            var aesKey = Decryption.DecryptKey("EFBE9CEFBE90D9B5EFBE88EFBEA543EFBEB447EFBEB0274C36285D7FEFBE9BEFBEA4D99EEFBF88EFBE8770EFBEAC10EFBF80");
            _bnsDat = new BNSDat.BNSDat { AES_KEY = aesKey, XOR_KEY = Decryption.XorKey };
            // Extract files
            var xml64List = new List<string>()
            {
                "client.config2.xml",
                "skill3_contextscriptdata_blademaster.xml",
                "skill3_contextscriptdata_blademaster_contextsimplemode.xml"
            };
            DatFolder = datFolder;
            var xml64Path = Path.Combine(datFolder, "xml64.dat");
            _xml64 = _bnsDat.ExtractFile(xml64Path, xml64List, xml64Path.Contains("64"));
        }

        public void LoadFolder(string path)
        {
            foreach (var file in new DirectoryInfo(path).GetFiles("*.dat"))
            {
                if (file != null)
                {
                    var directoryName = Path.GetDirectoryName(file.FullName);
                    if (!MyDictionary.ContainsKey(file.Name))
                    {
                        MyDictionary.Add(file.Name, directoryName);
                    }
                }
            }
        }

        public void LoadDataFile(string path)
        {
            var fileList = _bnsDat.GetFileList(path, path.Contains("64"));
        }

        public void SaveFile()
        {
            var xml64Path = Path.Combine(DatFolder, "xml64.dat");
            _bnsDat.CompressFiles(xml64Path, _xml64, xml64Path.Contains("64"), 1);
        }
    }
}
