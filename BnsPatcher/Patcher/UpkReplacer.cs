﻿using System.IO;

namespace BnsPatcher.Patcher
{
    public class UpkReplacer
    {
        public string ContentFolder { get; set; }

        public UpkReplacer(string contentFolder)
        {
            ContentFolder = contentFolder;
        }

        public void RemoveBmSpiritEffect()
        {
            File.Copy(@".\Upk\BM_Spirit_Effect_Remove\00013263.upk", Path.Combine(ContentFolder, @"bns\CookedPC", "00013263.upk"), true);
            File.Copy(@".\Upk\BM_Spirit_Effect_Remove\00060548.upk", Path.Combine(ContentFolder, @"bns\CookedPC", "00060548.upk"), true);
        }

        public void RestoreBmSpiritEffect()
        {
            File.Copy(@".\Upk\BM_Spirit_Effect_Restore\00013263.upk", Path.Combine(ContentFolder, @"bns\CookedPC", "00013263.upk"), true);
            File.Copy(@".\Upk\BM_Spirit_Effect_Restore\00060548.upk", Path.Combine(ContentFolder, @"bns\CookedPC", "00060548.upk"), true);
        }
    }
}
