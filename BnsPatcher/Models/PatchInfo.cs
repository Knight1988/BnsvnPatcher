﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BnsPatcher.Annotations;

namespace BnsPatcher.Models
{
    class PatchInfo : INotifyPropertyChanged 
    {
        public string ContentFolder { get; set; }
        public double Progress { get; set; }
        public string ProgressText { get; set; }
        public double ProgressMaximum { get; set; }
        public bool IsNotPatching { get; set; } = true;
        // Config
        public bool ShowDps { get; set; } = true;
        public bool FastExtract { get; set; } = true;
        public bool DisableAutoBias { get; set; } = true;
        public bool NoChatBan { get; set; } = true;
        public bool IncreaseDps { get; set; }
        // Config Crystal
        public bool DeleteCrystal { get; set; } = true;
        public bool DeleteCrystalDriver { get; set; }
        public bool RestoreCrystal { get; set; }
        // Skill Effect
        public bool DeleteBmSpiritEffect { get; set; } = true;
        // BM Spirit
        public bool BmSpiritAddBladeStormToSf3 { get; set; } = true;
        public bool BmSpiritOptimizeSimpleMode { get; set; } = true;
        public bool BmSpiritRemove4ThHitFallingStar { get; set; } = true;
        public bool BmSpiritMoveSwordFallToLmb { get; set; } = true;
        // Exception handle
        public Exception Exception { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
