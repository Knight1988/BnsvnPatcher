﻿using BnsPatcher.Patcher;
using Ookii.Dialogs.Wpf;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using BnsPatcher.Patcher.Extensions;

namespace BnsPatcher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DatEditor _datEditor;

        public MainWindow()
        {
            InitializeComponent();
#if DEBUG
            PatchInfo.ContentFolder = "F:\\Games\\Garena\\32834\\service\\contents";
#endif
        }

        private void BrowseDatFolderClick(object sender, RoutedEventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            var result = dialog.ShowDialog();
            if (result == true)
            {
                PatchInfo.ContentFolder = dialog.SelectedPath;
            }
        }

        private void Patch_OnClick(object sender, RoutedEventArgs e)
        {
            Task.Run(() => PatchAsync());
        }

        private async Task UpdateProgressAsync(double progress, double total)
        {
            await Task.Yield();
            PatchInfo.Progress = progress;
            PatchInfo.ProgressMaximum = total;
            PatchInfo.ProgressText = ((progress/total)*100).ToString("##.##");
        }

        /// <summary>
        /// Extract, patch and compile dat files
        /// </summary>
        private async void PatchAsync()
        {
            if (!Validate()) return;

            await Task.Run(async () =>
            {
                try
                {
                    PatchInfo.IsNotPatching = false;
                    const double total = 3;
                    await UpdateProgressAsync(0, total);

                    var datPath = Path.Combine(PatchInfo.ContentFolder, "Local\\GARENA\\data");
                    _datEditor = new DatEditor(datPath);

                    UpdateClientConfig();
                    await UpdateProgressAsync(1, total);
                    UpdateBmSpirit();
                    await UpdateProgressAsync(2, total);
                    ReplaceUpk();
                    await UpdateProgressAsync(3, total);
                    _datEditor.SaveFile();
                }
                catch (Exception e)
                {
                    PatchInfo.Exception = e;
                }
                finally
                {
                    PatchInfo.IsNotPatching = true;
                }
            });

            if (PatchInfo.Exception != null)
            {
                MessageBox.Show(PatchInfo.Exception.ToString());
                PatchInfo.Exception = null;
            }
            else
            {
                MessageBox.Show("Patch Successful");
            }
        }

        private void UpdateBmSpirit()
        {
            if (PatchInfo.BmSpiritOptimizeSimpleMode)
            {
                _datEditor.Remove4ThHitFallingStars();
            }

            if (PatchInfo.BmSpiritMoveSwordFallToLmb)
            {
                _datEditor.MoveSwordFallToLmb();
            }

            if (PatchInfo.BmSpiritOptimizeSimpleMode)
            {
                _datEditor.OptimizeSimpleMode();
            } 
            else if (PatchInfo.BmSpiritAddBladeStormToSf3)
            {
                _datEditor.AddBladeStormToSf3();
            }
        }

        private void UpdateClientConfig()
        {
            if (PatchInfo.ShowDps)
            {
                _datEditor.ShowDps();
            }

            if (PatchInfo.NoChatBan)
            {
                _datEditor.NoChatBan();
            }

            if (PatchInfo.FastExtract)
            {
                _datEditor.FastExtract();
            }

            if (PatchInfo.IncreaseDps)
            {
                _datEditor.IncreaseDps();
            }
        }

        private void ReplaceUpk()
        {
            var upkReplacer = new UpkReplacer(PatchInfo.ContentFolder);

            var crystalUpkLocation = Path.Combine(PatchInfo.ContentFolder, "bns\\CookedPC\\00060898.upk");
            if (PatchInfo.DeleteCrystal)
            {
                File.Copy(".\\Upk\\DeleteCrystal.upk", crystalUpkLocation, true);
            }
            else if (PatchInfo.DeleteCrystalDriver)
            {
                File.Copy(".\\Upk\\DeleteCrystalDriver.upk", crystalUpkLocation, true);
            }
            else
            {
                File.Copy(".\\Upk\\RestoreCrystal.upk", crystalUpkLocation, true);
            }

            if (PatchInfo.DeleteBmSpiritEffect)
            {
                upkReplacer.RemoveBmSpiritEffect();
            }
            else
            {
                upkReplacer.RestoreBmSpiritEffect();
            }
        }

        private bool Validate()
        {
            if (PatchInfo.ContentFolder == null)
            {
                MessageBox.Show("Chưa chọn thư mục Data");
                return false;
            }

            var xml64 = Path.Combine(PatchInfo.ContentFolder, "Local\\GARENA\\data\\xml64.dat");
            if (!File.Exists(xml64))
            {
                MessageBox.Show("Không tìm thấy file xml64.dat");
                return false;
            }
            return true;
        }
    }
}